/*
 * Project 1: Group4: Encryption Team
 * Company Name: Asclepian Secure Technologies
 * Copyright: Asclepian Secure Technologies 2018
 * File: asclepian_crypto.cpp
 * Purpose: Implementation for Group 4's encryption
 *
 * Major changes:
 *      Custom mode (in asclepian_crypto.cpp)
 *      Custom padding (in asclepian_crypto.cpp)
 */

#ifndef G4_ENCRYPTION_SOURCE_ASCLEPIAN_CRYPTO_H
#define G4_ENCRYPTION_SOURCE_ASCLEPIAN_CRYPTO_H

#include <cinttypes>    //u_int8
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include <string>       //std::string
#include <openssl/aes.h>    //aes
#include <openssl/rand.h>   //rand
#include <algorithm>        //rand

#define MAX_LENGTH 5120//5KB
#define ERROR -1       //error

using std::string;

string* encrypt(string encryptMe);

string decrypt(string decryptMe, string key1, string key2);

#endif //G4_ENCRYPTION_SOURCE_ASCLEPIAN_CRYPTO_H
