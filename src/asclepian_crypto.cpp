/*
 * Project 1: Group4: Encryption Team
 * Company Name: Asclepian Secure Technologies
 * Copyright: Asclepian Secure Technologies 2018
 * File: asclepian_crypto.cpp
 * Purpose: Implementation for Group 4's encryption
 *
 * Major changes:
 *      Custom mode
 *      Custom padding
 *
 * Usage (Command line):
 *      ./G4_encryption_source "This is a string that needs encrypted."
 *      ./<Executable_Name_Here>  <String_To_Encrypt_Here>
 *
 * Functionality of main():
 *      To demonstrate the abilities, the input string will be encrypted, encoded, printed;
 *      Then decoded, decrypted, printed.
 */

//The header for our Encryption Task in Project1
#include "asclepian_crypto.h"

/* Normally one might avoid including .cpp
 * However in this case has the exact effect desired - adding the code together
 * Usually, would just put them in the same file, however, I separated them out
 * to make attributing credit to the author of the base.cpp code easier.
 */
#include "base64.cpp"

/*
 * Function: Creates a randomly generated byte via RAND_bytes(...)
 * Parameters:
 *      N/A
 * Returns:
 *      One random byte that is not 'X', '\n' or '\0'
 *
 * 'X' is removed because it is used to indicate start of padding
 *  '\n' and '\0' were removed by implementation to make debugging via printing out strings easier
 *
 */
static uint8_t genRandPad()
{
    //random byte variable
    uint8_t tmp = 0;

    //take byte tmp, and put a random byte inside that is not X or \n or \0
    do{RAND_bytes(&tmp, sizeof(tmp));} while (tmp == 'X' || tmp == '\n' || tmp == '\0');

    //return random byte
    return tmp;
}

/*
 * Function: Takes plaintext string and pads it so it will divide evenly into AES_BLOCK_SIZE (16B)
 * Parameters:
 *      One plaintext string to be padded
 *      One integer number with the count of bytes that need padded
 * Returns:
 *      One plaintext padded string, that will divide evenly into AES blocks
 */
static string customPadding(string noPadData, unsigned int padBytes)
{

    //string conversion variable
    std::stringstream ss;

    //load stream
    ss << noPadData;

    //Implement padding: main loop
    for(int i = 0; i < padBytes; i++) {

        //if not first byte, generate random byte
        if(i != 0) {
            ss << genRandPad();
        }

        //if first byte load 'X'
        else {
            ss << 'X';
        }
    }

    //return basic::string object
    return ss.str();
}

/*
 * Function: Encrypts the passed in string, generates two keys
 * Parameters:
 *      One plaintext string to be encrypted
 * Returns:
 *      One  encoded aes_256 encrypted string (using Asclepian Crypto's custom mode and padding)
 */
string* encrypt(string encryptMe)
{

    //return variable
    auto retData = new string[3];

    //encoding variables
    int arraySize = 2*(encryptMe.length() + (AES_BLOCK_SIZE - (encryptMe.length() % AES_BLOCK_SIZE)));
    unsigned char encoder[arraySize];
    int inc = 0;

    //key variables
    auto EncryptAesKey = new AES_KEY();
    uint8_t key1[32];
    uint8_t key2[32];
    uint8_t fKey[32];

    //padding variables
    string padded;
    unsigned int padBytes;

    //encryption variables
    unsigned char in[16];
    unsigned char encryptedData[32] = {0};
    string encryptedString;
    const char* plainArr;

    //initialize/generate AES key1 key2
    RAND_bytes(key1, sizeof(key1));
    RAND_bytes(key2, sizeof(key2));

    //encode key1 key2 for storage/transmission
    retData[0] = base64_encode(key1, 32);
    retData[1] = base64_encode(key2, 32);

    //Padding implementation
    padBytes = AES_BLOCK_SIZE - (encryptMe.length() % AES_BLOCK_SIZE);
    padded = customPadding(encryptMe, padBytes);

    //string to const array, later assign to non-const array, per AES input requirement
    plainArr = padded.c_str();

    //XOR key1 key2
    for(int i = 0; i <32; i++) {
        fKey[i] = key1[i] ^ key2[i];
    }

    //set initial key
    AES_set_encrypt_key(fKey, 256, EncryptAesKey);

    //Implementation encryption: main loop
    for(int i = 0; i < padded.length()/AES_BLOCK_SIZE; i++) {

        //generate keys after initial key is used
        if(i > 0) {

            //xor ciphertext with previous key
            for(int t = 0; t < AES_BLOCK_SIZE; t++) {

                // use ciphertext twice 0-15
                fKey[t] = fKey[t] ^ encryptedData[t];

                //use ciphertext twice 16-31
                fKey[t+AES_BLOCK_SIZE] = fKey[t+AES_BLOCK_SIZE] ^ encryptedData[t];
            }

            //set current encryption key
            AES_set_encrypt_key(fKey, 256, EncryptAesKey);
        }

        //get next bytes to encrypt
        for (int j = 0; j < AES_BLOCK_SIZE; j++) {
            in[j] = plainArr[i*AES_BLOCK_SIZE + j];
        }

        //encrypt
        AES_encrypt(in, encryptedData, EncryptAesKey);

        //store encrypted byte for encoding later
        for(unsigned char ch : encryptedData) {
            encoder[inc] = ch;
            inc++;
        }
    }

    //encode the information
    retData[2] = base64_encode(encoder, arraySize);
    return retData;
}

/*
 * Function: Decrypts the passed in string, using the two keys
 * Parameters:
 *      One base64 encoded aes_256 encrypted string (using Asclepian Crypto's custom mode and padding)
 *      Two base64 encoded aes_256 randomly generated keys (passed in as strings)
 * Returns:
 *      One decoded decrypted (plaintext) string
 */
string decrypt(string cipherStr, string skey1, string skey2)
{

    //return and decoding variables
    string retData;
    string decodeStr;
    string s1;
    string s2;
    std::stringstream ss;

    //key variables
    auto DecryptAesKey = new AES_KEY();
    const char *key1;
    const char *key2;
    unsigned char fKey[32];

    //decryption variables
    unsigned char out[16];
    unsigned char decryptedData[16] = {0};
    const char* cipherArr;

    //decoding, initialize data to decrypt
    decodeStr = base64_decode(cipherStr);

    //string to const array, later assign to non-const array, per AES input requirement
    cipherArr = decodeStr.c_str();

    //decoding, initialize keys
    s1 = base64_decode(skey1);
    s2 = base64_decode(skey2);
    key1 = s1.c_str();
    key2 = s2.c_str();

    //initial XOR key1 key2
    for (int i = 0; i < 32; i++) {
        fKey[i] = key1[i] ^ key2[i];
    }

    //set initial key
    AES_set_decrypt_key(fKey, 256, DecryptAesKey);

    //Implementation decryption: note the main loop is divided by 2*AES_BLOCK to enable
    for(int i = 0; i < cipherStr.length()/(2*AES_BLOCK_SIZE); i++)
    {

        //generate keys after initial key is used
        if(i > 0) {

            //xor ciphertext with previous key
            for(int t = 0; t < AES_BLOCK_SIZE; t++) {

                // use ciphertext twice 0-15
                fKey[t] = fKey[t] ^ out[t];

                // use ciphertext twice 16-31
                fKey[t+AES_BLOCK_SIZE] = fKey[t+AES_BLOCK_SIZE] ^ out[t];
            }

            //set current decryption key
            AES_set_decrypt_key(fKey, 256, DecryptAesKey);
        }

        //get next bytes to decrypt; note the multiple of 2; tied to division in main loop
        for (int j = 0; j < AES_BLOCK_SIZE; j++) {
            out[j] = cipherArr[2*i*AES_BLOCK_SIZE + j];
        }

        //run the decryption on out[16], put in decryptedData[16]
        AES_decrypt(out, decryptedData, DecryptAesKey);

        //store decrypted bytes
        for(unsigned char w : decryptedData) {
            ss << w;
        }
    }

    //convert string, remove pad, and return
    retData = ss.str();
    retData = retData.substr(0, retData.find_last_of('X'));
    return retData;
}

//See 'Usage' and 'Functionality' in initial comment for this file.
int main(int argc, char *argv[])
{
    //Input and print-out variables
    string data;
    string* encrypted;
    string decrypted;

    //Make sure command line call has required argument
    if (argc > 1) {
        string arg1(argv[1]);
        data = arg1;

        //if arg[1] is empty then return error
        if(!data.empty()) {
            if(data.length() > MAX_LENGTH) {
                std::cout << "Input string over " << MAX_LENGTH << " in length." << std::endl;
                return ERROR;
            }
        }
    }

    //if no argument at all, return error
    else {
        std::cout << "Incorrect args, argc was less than one (1)." << std::endl;
        return ERROR;
    }

    //encrypt plaintext-string
    encrypted = encrypt(data);
    std::cout << "Encoded & Encrypted String (length): " << encrypted[2] << " ("<< encrypted[2].length()<<")"<< std::endl;

    //decrypt encoded-string
    decrypted = decrypt(encrypted[2],encrypted[1],encrypted[0]);

    std::cout << "Recovered String (length): " << decrypted << " ("<< decrypted.length()<<")"<< std::endl;
    return 0;
}
