# G4 Encryption Project

## Description

The G4 Encryption Project is an academic group effort at Wright State University in Beavercreek, Ohio. The corresponding course is CEG4110-01 held in the fall semester of 2018.

Project is purely *academic* and is not intended for commercial use.

## Objective

The G4 Encryption Project aims develop an encryption method for transferring medical data from a personal device to a specified location.

Project builds off the existing encryption library in [OpenSSL](https://www.openssl.org/). Two significant changes are made per request that differs from OpenSSL library.

## Contributing

The G4 Encryption Project has concluded its academic purpose and is now open to the public. Currently, the developers of this repository are no longer supporting any modifications. However, this software is free is reference and use.
