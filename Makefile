CC=g++
RM=rm -f
MV=mv

SOURCE=src
INCLUDE=include
EXTERNAL_LIBS=ext

default_target: asclepian_crypto

asclepian_crypto: openssl/crypto.a base64.o
	$(CC) -std=c++11 -I${INCLUDE} -I$(EXTERNAL_LIBS) -I$(EXTERNAL_LIBS)/openssl/include -L$(EXTERNAL_LIBS)/openssl $(SOURCE)/asclepian_crypto.cpp base64.o -lcrypto -oasclepian_crypto

openssl/crypto.a:
	$(MAKE) -C $(EXTERNAL_LIBS)/openssl

base64.o:
	$(CC) -c $(EXTERNAL_LIBS)/base64.cpp -obase64.o

clean:
	$(MAKE) -C $(EXTERNAL_LIBS)/openssl clean
	$(RM) `find . -name '*.[oa]' -a \! -path "./.git/*"`
	$(RM) asclepian_crypto
